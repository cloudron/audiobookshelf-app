This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>

Content can be uploaded into `/app/data/podcasts` and `/app/data/audiobooks`.

