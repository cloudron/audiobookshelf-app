#!/bin/bash

set -eu

mkdir -p /app/data/audiobooks /app/data/podcasts

# https://www.audiobookshelf.org/docs#env-configuration
export PORT=8000
export HOST=0.0.0.0
export SOURCE=cloudron
export CONFIG_PATH=/app/data/config
export METADATA_PATH=/app/data/meta

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

wait_for_audiobookshelf() {
    # Wait for app to come up
    while ! curl --fail -s http://localhost:8000 > /dev/null; do
        echo "=> Waiting for app to come up"
        sleep 1
    done
}

# sqlite3 does not allow multiple writes in WAL mode. This means that we cannot update settings in the background
# https://www.sqlite.org/wal.html (concurrency section, 3rd para)
if [[ ! -f "/app/data/config/absdatabase.sqlite" ]]; then
    echo "==> Creating database on first run"
    /usr/local/bin/gosu cloudron:cloudron node /app/code/index.js &
    pid=$!
    sleep 5

    wait_for_audiobookshelf

    admin_count=$(sqlite3 /app/data/config/absdatabase.sqlite "SELECT count(*) FROM users WHERE type='root' AND isActive=true")
    if [[ "${admin_count}" == "0" ]]; then
        echo "=> First run. Creating admin user"
        curl -s -X POST -H 'content-type: application/json' -d '{"newRoot":{"username":"admin","password":"changeme"}}' 'http://localhost:8000/init'
    fi
    sleep 2

    echo "==> Stopping after database creating"
    kill -SIGTERM ${pid} # this kills the process group
fi

function setup() {
    echo "=> Updating SMTP details"
    email_settings=$(sqlite3 /app/data/config/absdatabase.sqlite "SELECT value FROM settings WHERE key='email-settings'")
    if [[ -z $email_settings ]]; then
        email_settings="{\"id\":\"email-settings\",\"host\":\"\",\"port\":2525,\"secure\":false,\"user\":\"\",\"pass\":\"\",\"testAddress\":null,\"fromAddress\":\"\",\"ereaderDevices\":[]}"
    fi
    email_settings=$(echo $email_settings \
        | jq ".host=\"${CLOUDRON_MAIL_SMTP_SERVER}\"" \
        | jq ".port=\"$CLOUDRON_MAIL_SMTP_PORT\"" \
        | jq ".secure=false" \
        | jq ".user=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" \
        | jq ".pass=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" \
        | jq -c ".fromAddress=\"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Audiobookshelf} <${CLOUDRON_MAIL_FROM}>\"")

    sqlite3 /app/data/config/absdatabase.sqlite "REPLACE INTO settings (key, value, createdAt, updatedAt) VALUES ('email-settings', '$email_settings', DATETIME('now'), DATETIME('now'))"
}

function oidc_setup() {
    echo "=> OIDC configuration"
    provider_name="${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}"
    server_settings_w_oidc_config=$(sqlite3 /app/data/config/absdatabase.sqlite "SELECT value FROM settings WHERE key='server-settings'" \
        | jq ".authActiveAuthMethods=[\"local\", \"openid\"]" \
        | jq ".authOpenIDIssuerURL=\"${CLOUDRON_OIDC_ISSUER}\"" \
        | jq ".authOpenIDAuthorizationURL=\"${CLOUDRON_OIDC_AUTH_ENDPOINT}\"" \
        | jq ".authOpenIDTokenURL=\"${CLOUDRON_OIDC_TOKEN_ENDPOINT}\"" \
        | jq ".authOpenIDUserInfoURL=\"${CLOUDRON_OIDC_PROFILE_ENDPOINT}\"" \
        | jq ".authOpenIDJwksURL=\"${CLOUDRON_OIDC_KEYS_ENDPOINT}\"" \
        | jq ".authOpenIDLogoutURL=null" \
        | jq ".authOpenIDClientID=\"${CLOUDRON_OIDC_CLIENT_ID}\"" \
        | jq ".authOpenIDClientSecret=\"${CLOUDRON_OIDC_CLIENT_SECRET}\"" \
        | jq ".authOpenIDLogoutURL=\"${CLOUDRON_APP_ORIGIN}\"" \
        | jq ".authOpenIDButtonText=\"Login with ${provider_name//\'/\'\'}\"" \
        | jq ".authOpenIDAutoLaunch=false" \
        | jq ".authOpenIDSubfolderForRedirectURLs=\"\"" \
        | jq ".authOpenIDAutoRegister=true" \
        | jq -c ".authOpenIDMatchExistingBy=\"email\"")

    sqlite3 /app/data/config/absdatabase.sqlite "UPDATE settings SET value='$server_settings_w_oidc_config' WHERE key='server-settings'"
}

( setup )

[[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]] && ( oidc_setup )

echo "==> Starting Audiobookshelf"
exec /usr/local/bin/gosu cloudron:cloudron node /app/code/index.js
