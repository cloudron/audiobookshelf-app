[0.1.0]
* Initial version for Audiobookshelf

[0.2.0]
* Pre-setup /app/data/libraries as data folder

[0.3.0]
* Change code layout

[0.4.0]
* Create `podcasts` and `audiobooks` folders to match upstream docs
* Move config and meta directory one level up

[1.0.0]
* Initial stable release

[1.1.0]
* Update Audiobookshelf to 2.3.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.3.0)
* Epub ereader settings for font scale, line spacing, theme and spread
* Option to show full ffprobe output in "More Info" modal of audio files
* Library setting to hide single book series #1433
* Mark all episodes as finished option on podcast item page #1862
* Library filter for publisher #1813

[1.1.1]
* Update Audiobookshelf to 2.3.1
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.3.1)
* Reduced memory needed to migrate that was causing crash
* Several issues with db migration #1913
* Opening series page on mobile #1914
* First library opened on launch #1911
* Playing downloaded media items in mobile apps #1912

[1.1.2]
* Update Audiobookshelf to 2.3.2
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.3.2)
* Crash when updating series sequence #1919
* Crash on playing downloaded media from mobile apps #1912
* Reduced memory usage on load & increase max memory size #1926 #1913

[1.1.3]
* Update Audiobookshelf to 2.3.3
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.3.3)
* Reduced memory usage on initialization causing OOM crashes #1926 #1931 #1938
* Creating a new user with empty list of tags #1933
* Crash when new author is set without a name #1934

[1.2.0]
* Update Audiobookshelf to 2.4.1
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.4.0)
* Config page to manage all open RSS feeds by @shawnphoffman in #2026
* Lithuanian translations by @petras-sukys in #1965
* Get all items API endpoint returning no items when limit is not specified #2067
* Server crash when browsing folders in library modal #2065
* Server crash when quick matching using find covers server setting #2068
* Server crash when sorting by title in podcast library #2069

[1.2.1]
* Update Audiobookshelf to 2.4.2
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.4.1)
* Get all items API endpoint returning no items when limit is not specified #2067
* Server crash when browsing folders in library modal #2065
* Server crash when quick matching using find covers server setting #2068
* Server crash when sorting by title in podcast library #2069

[1.2.2]
* Update Audiobookshelf to 2.4.3
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.4.3)
* Duplicate authors and series being created #2106
* Scanner purge old cover image cache when new cover is found
* Slow queries on server init that check for invalid db records by @selfhost-alt in #2103
* Server crash when parsing full names by @selfhost-alt in #2101
* Server crash when removing an item from a playlist #2115

[1.2.3]
* Update Audiobookshelf to 2.4.4
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.4.4)
* Fuzzy matching for metadata providers #396 by @mikiher in #2099
* Norwegian translations by @husjon in #2138
* Delete author button shown on edit author modal #2124

[1.3.0]
* Update Audiobookshelf to 2.5.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.5.0)
* Scanner settings tab in the edit library modal to manage the scanners metadata precedence (See scanner guide)
* Tools tab in the edit library modal to batch remove all metadata.json or metadata.abs files in library folders
* Support for pasting semicolon separated strings into multi-select inputs #1198
* Scanner recognizes ASIN when inside brackets. e.g. [B002UZJGYY] #1852
* Epub ereader setting to change font family (serif/sans-serif) by @MxMarx in #2253
* Epub ereader search for text within epubs in the TOC sidemenu #2045 by @MxMarx in #2255
* Ereader device setting to allow users to send ebooks to device #1982

[1.4.0]
* Update Audiobookshelf to 2.6.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.6.0)
* Simple API caching for /libraries* requests by @mikiher in #2343
* NFO files as book metadata source by @mikiher in #2305
* Czech translations
* Swedish translations

[1.5.0]
* Enable OIDC integration

[1.6.0]
* Update Audiobookshelf to 2.7.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.7.0)
* Year in review stats images to user stats page #2373
* Ability to fetch book data in uploader by @kieraneglin in #2333
* Zoom controls to comic reader by @pablojimenezmateo in #2431
* Vulnerability report https://github.com/advplyr/audiobookshelf/security/advisories/GHSA-gjgj-98v3-47pg and https://github.com/advplyr/audiobookshelf/security/advisories/GHSA-jhjx-c3wx-q2x7
* Cache not clearing for homepage shelves when media progress is updated #2392
* Narrators page 404 on page reload #2359
* Hide Change Password when Password Authentication is disabled #2367
* Server crash when user created through OIDC attempts password login #2378
* Issue parsing some RSS feeds that require accept header #2401
* RSS feeds for collections not updating when adding/removing books #2414
* Cover size widget hidden behind audio player #2443
* Listening sessions table allows multi-select to delete, sorting with column headers & changing rows per page
* Initial startup database cleanup includes removing playback sessions that have a listening time of 3s or less
* Sort audible match results by closest duration #2238 and other match improvements by @mikiher in #2400
* Audible series sequences that include spaces will only take the first part of the string (and commas removed) #2380
* SSO/OpenID: Use a mobile-redirect route (Fixes #2379 and #2381) by @Sapd in #2386
* SSO/OpenID: Provide error messages to logs by @Sapd in #2365
* Sample Docker Compose file comments by @nichwall in #2415
* Readme update for synology reverse proxy by @treyg in #2420

[1.6.1]
* Update Audiobookshelf to 2.7.1
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.7.1)
* Podcast performance server side: Slow queries for podcasts with many episodes #2073
* Podcast performance client side: Slow/unusable podcast page when podcast has many episodes (added lazy loading) #1549
* Playlist navigation button not showing on mobile screens #2469
* Server crash when file is added and removed quickly #2332 by @thevoltagesource in #2465
* Merging chapters from multiple audio files when audio files have the same chapter titles #2461
* Unable to fetch RSS feed for specific podcasts (updated request header) #2446
* Book scanner not updating sequence if only the sequence was changed
* Podcast episode showing 0 seconds remaining when toggling finished

[1.6.2]
* Update Audiobookshelf to 2.7.2
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.7.2)
* Scanner parses epub metadata and extracts cover art #1479
* Scanner parses comic metadata (limited) and extracts cover art #1837
* Playlist button to podcast episodes on latest page #2455

[1.7.0]
* Update Audiobookshelf to 2.8.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.8.0)

[1.7.1]
* Fix mobile OpenID login

[1.7.2]
* Update Audiobookshelf to 2.8.1
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.8.1)
* Book library setting to skip earlier books in Continue Series home page shelf #2687 by @justcallmelarry in #2737
* Sorting options to authors page #2547 by @KeyboardHammer in #2580
* Keyboard navigation for multi-select dropdown inputs #2555 by @mikiher in #2721
* Book library filters for "Has no ebook" and "Has no supplementary ebook" #1889 by @Teekeks in #2677
* Abridged checkbox to batch edit page overwrite map details box #2695
* Traditional Chinese translations by @den13501 in #2732
* Vietnamese translations (thanks saigonviral in Discord!) by @nichwall @2726
* Hungarian translations by @megamegax in #2636
* Estonian translations by @RasmusKoit in #2644

[1.8.0]
* Update Audioboookshelf to 2.9.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.9.0)
* OpenID Connect Auth supports setting user type and permissions #2523 (also fix #2440) by @Sapd in #2769
* OpenID Connect Auth supports setting signing algorithm by @apocer in #2820
* Podcast library filter for language
* Support for webp images in cbz/cbr comic files #2792
* Hebrew translations by @arcmagedr #2756
* Ukrainian translations by @cor-bee in #2757
* Bengali translations by @soaibsafi in #2840
* Podcast episode downloads failing to download with some embedded cover art (now excluding non-audio streams) #2858
* Library "Match Books" only running on first 100 books #2096
* metadata.json files not being updated when using item metadata utils #2837
* Enabling/disabling library watcher not taking effect until server restart #2775
* Local (offline) listening sessions from mobile clients set on the wrong day #2795
* Scanner issue after renaming/moving files #2686 #2767 by @mikiher in #2773
* Server crash when scanning bad epub file #2856
* Server crash when matching an item with cover and cover fails to download #2857
* Server crash when using a custom metadata provider by @rasmuslos in #2784
* Server crash when matching a book and the author name ends in a comma #2796
* "Skip earlier books in Continue Series" library setting returning less than 10 books by @justcallmelarry in #2789
* Podcast episode audio file meta tag for "subtitle" being used for episode description over "description" meta tag #2843
* UI/UX: Home page default view always showing horizontal scrollbar
* UI/UX: Audio player playback icons overlapping on smaller screen #2799
* UI/UX: Multiple series on book page not showing comma separator by @lkiesow in #2821

[1.9.0]
* Update Audioboookshelf to 2.10.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.10.0)
* Bulgarian translations by @lembata in #2966
* Users unable to edit their playlists #3016 by @nichwall in #3017
* Embedded chapters not sorted when probed from audio files #3007
* Image in fullscreen cover modal not updating on cover change #2900
* Switching libraries while viewing a series page shows "No results" (now redirects to series home) #2902
* Users created with leading/trailing whitespace cannot login (usernames are now trimmed) #2882
* Email settings not using port 465 failing to connect #2765
* Server crash when transcodes fail to write concat file
* Inconsistent progress bars shown on series & collapsed series #2921 by @mikiher in #2954
* Incorrect order of author names on book cards #2859

[1.9.1]
* Update Audioboookshelf to 2.10.1
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.10.1)
* Edit author modal resetting all form inputs on image changes #2965
* Improved performance for loading playlists #2852

[1.10.0]
* Add email display name support

[1.11.0]
* Update Audioboookshelf to 2.11.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.11.0)

[1.12.0]
* Update Audioboookshelf to 2.12.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.12.0)

[1.12.1]
* Update Audioboookshelf to 2.12.1
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.12.1)
* Server crash loading unicode.so sqlite extension #3231 by @devnoname120 in #3236

[1.12.2]
* Update Audioboookshelf to 2.12.2
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.12.2)
* Revert session MemoryStore back to default #3251
* Podcast episode download request failing due to user-agent string #3246
* UNC path normilization #640 by @mikiher in #3254
* Changelog modal shows all releases with the same major/minor version #3250
* Random sort option skips caching #3249
* More strings translated #3247 

[1.12.3]
* Update Audioboookshelf to 2.12.3
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.12.3)
* Localization for year in review images by @Vito0912 in #3262
* Add log for library item download by @ic1415 in #3245
* Reverted accent-insensitive search (original issue #2678)

[1.12.4]
* Update nodejs version to 18

[1.13.0]
* Update Audioboookshelf to 2.13.1
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.13.1)
* Slovenian language option
* Server crash when adding new podcasts or uploading new items #3353
* More strings translated 

[1.13.1]
* Update Audioboookshelf to 2.13.2
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.13.2)
* New authors not setting lastFirst column
* Toasts not showing when updating library items

[1.13.2]
* Update Audioboookshelf to 2.13.3
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.13.3)
* User permissions inverted tags accessible not being saved #3365 by @nichwall in #3368
* Unable to update root user email #3366
* "Unlink OpenID" button showing blank in user account modal

[1.13.3]
* Update Audioboookshelf to 2.13.4
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.13.4)
* Metadata embed tool failing to embed webp covers (now converts to jpeg) #3379 by @mikiher in #3394
* Server crash when quick match adds a new series #3392 by @mikiher in #3395
* Server crash when using the get all collections API endpoint #3372
* Server crash when using a custom metadata provider that returns an invalid response #3390 by @mikiher in #3396
* Byte conversion for file sizes using 1024 instead of 1000 #3386

[1.14.0]
* Update Audioboookshelf to 2.14.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.14.0)

[1.15.0]
* Update Audioboookshelf to 2.15.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.15.0)
* Book library filter for published decade #3448 by @glorenzen in #3489
* Book library filter for missing chapters #3497
* Database constraint for unique series names in series table w/ migration (fixes #3207) by @nichwall in #3417

[1.15.1]
* Update Audioboookshelf to 2.15.1
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.15.1)
* Potential database corruptions from NOCASE indexes (migration) #3276 by @mikiher in #3533
* Next/previous chapter behavior on public share player by @mikiher in #3508
* Ignore dot files in migrations folder #3510
* Book library published decade filter by @glorenzen in #3518
* Extracting cover image from audio file with multiple images #2316 by @asoluter in #3529

[1.16.0]
* Update Audioboookshelf to 2.16.0
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.16.0)
* Library setting to control when to mark items as finished (time remaining or percentage) #837
* User permission to allow users to create ereaders #1982 by @laustindasauce in #3531
* Incorrect version shown as latest version when update is available
* Embed metadata tool embeds grouping tag as semicolon delimited & support for parsing multiple series in grouping tag #3473
* Improve performance w/ db migration for indexes (fixes #3259 #3525 #3237) by @nichwall in #3536
* Localization for missing strings #3544

[1.16.1]
* Update Audioboookshelf to 2.16.1
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.16.1)
* Some libraries created with older versions of the server unable to update library settings #3559
* Server crash when downloading files #3344 by @mikiher in #3553

[1.16.2]
* Update Audioboookshelf to 2.16.2
* [Full changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.16.2)
* Incorrect time remaining for podcast episodes #3560
* Server crash when streaming download by @mikiher in #3565


[1.17.0]
* Update audiobookshelf to 2.17.1
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.17.1)
* Server crash on new libraries when building filter data [#&#8203;3623](https://github.com/advplyr/audiobookshelf/issues/3623)
* Error adding new series when a series exists in the database with a `null` title [#&#8203;3622](https://github.com/advplyr/audiobookshelf/issues/3622)
* Db migration for non-matching UUID data types on associated models by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;3597](https://github.com/advplyr/audiobookshelf/issues/3597)
* Global search menu item unclickable from trackpad
* Server crash when `migrationsMeta` table is not populated by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;3589](https://github.com/advplyr/audiobookshelf/issues/3589)
* Heavy memory usage in podcast episode downloader (fixes OOM server crash [#&#8203;3601](https://github.com/advplyr/audiobookshelf/issues/3601)) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3604](https://github.com/advplyr/audiobookshelf/issues/3604)
* Podcast episodes duplicated when a scan runs while the episode is downloading [#&#8203;2785](https://github.com/advplyr/audiobookshelf/issues/2785)
* Series Progress filters incorrect - showing for any users progress [#&#8203;2923](https://github.com/advplyr/audiobookshelf/issues/2923)
* Unable to download CBC Radio podcasts due to them rejecting our user agent [#&#8203;3322](https://github.com/advplyr/audiobookshelf/issues/3322)
* Book library sorting by published year treats year as string (now casting to integer in query) [#&#8203;3620](https://github.com/advplyr/audiobookshelf/issues/3620)
* Improve cover image & author image performance by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3580](https://github.com/advplyr/audiobookshelf/issues/3580) [#&#8203;3584](https://github.com/advplyr/audiobookshelf/issues/3584)
* Improve performance w/ in-memory user cache by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3599](https://github.com/advplyr/audiobookshelf/issues/3599)
* Improve performance w/ library filter data by increasing cache time by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;3594](https://github.com/advplyr/audiobookshelf/issues/3594)
* Simplified query on library item updates to reduce memory usage by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3615](https://github.com/advplyr/audiobookshelf/issues/3615)
* `mpg` and `mpeg` added to supported audio file extensions by [@&#8203;4ch1m](https://github.com/4ch1m) in [#&#8203;3574](https://github.com/advplyr/audiobookshelf/issues/3574)
* Avoid parsing first and last names in Chinese, Japanese and Korean languages by [@&#8203;snakehnb](https://github.com/snakehnb) in [#&#8203;3585](https://github.com/advplyr/audiobookshelf/issues/3585)
* More strings translated
* Update user directive in sample docker compose by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;3568](https://github.com/advplyr/audiobookshelf/issues/3568)
* GH Workflow: Only run CodeQL and Integration actions if code changed by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;

[1.17.1]
* Update audiobookshelf to 2.17.2
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.17.2)
* Unable to edit book series sequence [#&#8203;3636](https://github.com/advplyr/audiobookshelf/issues/3636)
* iOS unable to select m4b files in file picker [#&#8203;2690](https://github.com/advplyr/audiobookshelf/issues/2690) by [@&#8203;sevenlayercookie](https://github.com/sevenlayercookie) in [#&#8203;3632](https://github.com/advplyr/audiobookshelf/issues/3632)
* Servers on v2.10.1 and below unable to upgrade to v2.17.0 and up (fixed migration file)
* Persist podcast episode table sort and filter options in browser local storage [#&#8203;1321](https://github.com/advplyr/audiobookshelf/issues/1321)
* UI/UX: Upload page cleaned up for mobile screen sizes
* More strings translated
* MR Template by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;3603](https://github.com/advplyr/audiobookshelf/issues/3603)
* [@&#8203;sevenlayercookie](https://github.com/sevenlayercookie) made their first contribution in https://github.com/advplyr/audiobookshelf/pull/3632

[1.17.2]
* Update audiobookshelf to 2.17.3
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.17.3)
* Db migration to fix foreign key constraints (See [#&#8203;3664](https://github.com/advplyr/audiobookshelf/issues/3664))
* Server crash deleting library that has playback sessions [#&#8203;3634](https://github.com/advplyr/audiobookshelf/issues/3634)
* Server crash when deleting user that has playback sessions
* API: Get media progress for podcast episode returning the library items progress
* Share player settings modal not functional by [@&#8203;glorenzen](https://github.com/glorenzen) in [#&#8203;3655](https://github.com/advplyr/audiobookshelf/issues/3655)
* Set Content-Security-Policy header to disallow iframes (thanks [@&#8203;alex-higham](https://github.com/alex-higham))
* View podcast episode modal includes audio filename and size at the bottom [#&#8203;3648](https://github.com/advplyr/audiobookshelf/issues/3648)
* More strings translated
* Updated readme with web client demo: https://audiobooks.dev/ (login with demo/demo) by [@&#8203;Vito0912](https://github.com/Vito0912)

[1.80.0]
* Checklist added to CloudronManifest
* Make use of COUDRON_OIDC_PROVIDER_NAME
* Use sqlite addon

[1.80.1]
* Update audiobookshelf to 2.17.5
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.17.5)
* Server setting to allow embedding the web client in an iframe (or use env variable `ALLOW_IFRAME=1`, see https://github.com/advplyr/audiobookshelf/issues/3684#issuecomment-2526175255) [#&#8203;3684](https://github.com/advplyr/audiobookshelf/issues/3684)
* Catalan language option
* Server crash on uploadCover temp file mv failed [#&#8203;3685](https://github.com/advplyr/audiobookshelf/issues/3685)
* Server crash when a playback session has no mediaMetadata object by [@&#8203;Vito0912](https://github.com/Vito0912) in [#&#8203;3689](https://github.com/advplyr/audiobookshelf/issues/3689)

[1.80.2]
* support single quotes in CLOUDRON_OIDC_PROVIDER_NAME

[1.80.3]
* Update audiobookshelf to 2.17.6
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.17.6)
* Option to enable downloading for media item shares [#&#8203;3606](https://github.com/advplyr/audiobookshelf/issues/3606) by [@&#8203;glorenzen](https://github.com/glorenzen) in [#&#8203;3666](https://github.com/advplyr/audiobookshelf/issues/3666)
* Year in Review dropdown to select year by [@&#8203;Vito0912](https://github.com/Vito0912) in [#&#8203;3717](https://github.com/advplyr/audiobookshelf/issues/3717)
* Quick match w/ override enabled does not remove empty series or authors [#&#8203;3743](https://github.com/advplyr/audiobookshelf/issues/3743)
* Podcast download queue page not available as a tab on mobile screens [#&#8203;3719](https://github.com/advplyr/audiobookshelf/issues/3719)
* Downloading podcast episode when file extension is mp3 but enclosure type is not mp3 (workaround for incorrect RSS feed) [#&#8203;3711](https://github.com/advplyr/audiobookshelf/issues/3711)
* File watcher ignores dot files but still polls them [#&#8203;3230](https://github.com/advplyr/audiobookshelf/issues/3230)
* UI/UX: RSS feed preview config page cover image aspect ratio broken on Safari [#&#8203;3748](https://github.com/advplyr/audiobookshelf/issues/3748) by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;3751](https://github.com/advplyr/audiobookshelf/issues/3751)
* Downloaded podcast episodes trim whitespace for descriptions [#&#8203;3720](https://github.com/advplyr/audiobookshelf/issues/3720)
* Migrate to new Feed/FeedEpisode model and remove old. See [#&#8203;3721](https://github.com/advplyr/audiobookshelf/issues/3721) [#&#8203;3724](https://github.com/advplyr/audiobookshelf/issues/3724)
* Support `SSRF_REQUEST_FILTER_WHITELIST` env variable [#&#8203;3742](https://github.com/advplyr/audiobookshelf/issues/3742)
* Added Australia and New Zealand podcast regions by [@&#8203;brinlyau](https://github.com/brinlyau) in [#&#8203;3727](https://github.com/advplyr/audiobookshelf/issues/3727)
* No compression when downloading library item as zip file [#&#8203;3081](https://github.com/advplyr/audiobookshelf/issues/3081) by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;3714](https://github.com/advplyr/audiobookshelf/issues/3714)
* UI/UX: Optimized rendering of LazyBookshelf, especially during scrolling by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3726](https://github.com/advplyr/audiobookshelf/issues/3726)
* UI/UX: Keyboard navigation for multi select inputs by [@&#8203;glorenzen](https://github.com/glorenzen) in [#&#8203;3575](https://github.com/advplyr/audiobookshelf/issues/3575)
* UI/UX: Accessibility updates for web client [#&#8203;2268](https://github.com/advplyr/audiobookshelf/issues/2268) [#&#8203;3699](https://github.com/advplyr/audiobookshelf/issues/3699)
* UI/UX: Modals gain focus when opened
* More strings translated

[1.80.4]
* Update audiobookshelf to 2.17.7
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.17.7)
* Feed episode IDs changing anytime the feed is refreshed [#&#8203;3757](https://github.com/advplyr/audiobookshelf/issues/3757). See [#&#8203;3772](https://github.com/advplyr/audiobookshelf/issues/3772)
* Feed not being refreshed in some cases. See [#&#8203;3772](https://github.com/advplyr/audiobookshelf/issues/3772)
* Share media player not using the media session API [#&#8203;3768](https://github.com/advplyr/audiobookshelf/issues/3768). See [#&#8203;3769](https://github.com/advplyr/audiobookshelf/issues/3769)
* UI/UX: User stats heatmap day color tints using entire listening history as range instead of just the last year
* UI/UX: User stats heatmap caption incorrect. See [#&#8203;3773](https://github.com/advplyr/audiobookshelf/issues/3773)
* Performance update for library page queries by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3767](https://github.com/advplyr/audiobookshelf/issues/3767)

[1.81.0]
* Update audiobookshelf to 2.18.0
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.18.0)

[1.81.1]
* Update audiobookshelf to 2.18.1
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.18.1)
* Epub ebooks fail to load when using subdirectory [#&#8203;3865](https://github.com/advplyr/audiobookshelf/issues/3865)
* 404 not found for texture image when using subdirectory [#&#8203;385](https://github.com/advplyr/audiobookshelf/issues/385) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3864](https://github.com/advplyr/audiobookshelf/issues/3864)
* 404 not found for libraries with uppercase UUID
* Collection & series rss feeds not including episodes. in [#&#8203;3867](https://github.com/advplyr/audiobookshelf/issues/3867)
* Added support for MRSS feeds [#&#8203;3695](https://github.com/advplyr/audiobookshelf/issues/3695) by [@&#8203;Timtam](https://github.com/Timtam) in [#&#8203;3732](https://github.com/advplyr/audiobookshelf/issues/3732)
* Generated rss feed episodes `itunes:duration` tag now sets duration in seconds (integer). in [#&#8203;3867](https://github.com/advplyr/audiobookshelf/issues/3867)
* Generated rss feeds `itunes:summary` tag wraps contents in `<![CDATA[]]>`. in [#&#8203;3867](https://github.com/advplyr/audiobookshelf/issues/3867)
* Generated rss feeds no longer include empty tags (e.g. <itunes:season/>). in [#&#8203;3867](https://github.com/advplyr/audiobookshelf/issues/3867)
* More strings translated
* [@&#8203;Timtam](https://github.com/Timtam) made their first contribution in https://github.com/advplyr/audiobookshelf/pull/3732

[1.82.0]
* Update audiobookshelf to 2.19.0
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.19.0)
* Support rich text book descriptions [#&#8203;1820](https://github.com/advplyr/audiobookshelf/issues/1820) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3880](https://github.com/advplyr/audiobookshelf/issues/3880)
* Player setting to adjust playback rate increment/decrement amount [#&#8203;3556](https://github.com/advplyr/audiobookshelf/issues/3556) by [@&#8203;glorenzen](https://github.com/glorenzen) in [#&#8203;3892](https://github.com/advplyr/audiobookshelf/issues/3892)
* Server crash for on failed extract epub image [#&#8203;3889](https://github.com/advplyr/audiobookshelf/issues/3889)
* Server crash on quick match [#&#8203;3883](https://github.com/advplyr/audiobookshelf/issues/3883) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3898](https://github.com/advplyr/audiobookshelf/issues/3898)
* Unable to download podcast episodes with the same name by [@&#8203;tharvik](https://github.com/tharvik) in [#&#8203;3906](https://github.com/advplyr/audiobookshelf/issues/3906)
* Collection/series feeds not incrementing pubdate correctly [#&#8203;3442](https://github.com/advplyr/audiobookshelf/issues/3442)
* Fatal logs not saving to crash_logs.txt [#&#8203;3919](https://github.com/advplyr/audiobookshelf/issues/3919)
* UI/UX: Collapsed sub series showing parent series name on hover instead of collapsed series name [#&#8203;3713](https://github.com/advplyr/audiobookshelf/issues/3713)
* API: Basepath (/audiobookshelf) was being included in track `contentUrl` (see [#&#8203;3921](https://github.com/advplyr/audiobookshelf/issues/3921))
* Add populate map details buttons to batch edit page
* Allows setting of some pragma values through environment variables [#&#8203;3750](https://github.com/advplyr/audiobookshelf/issues/3750) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3899](https://github.com/advplyr/audiobookshelf/issues/3899)
* UI/UX: Standardized clipboard copy buttons to show checkmark when copied
* UI/UX: Progress bar on covers has box shadow for visbililty [#&#8203;3825](https://github.com/advplyr/audiobookshelf/issues/3825) in [#&#8203;3914](https://github.com/advplyr/audiobookshelf/issues/3914)
* UI/UX: Add collection and playlist help text [#&#8203;3318](https://github.com/advplyr/audiobookshelf/issues/3318) by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;3916](https://github.com/advplyr/audiobookshelf/issues/3916)
* API: Expanded books include `descriptionPlain` that is stripped of html tags
* More strings translated
* Readme update Apache reverse proxy example by [@&#8203;adjokic](https://github.com/adjokic) in [#&#8203;3884](https://github.com/advplyr/audiobookshelf/issues/3884)
* Add: workflow to close blank issues by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;3907](https://github.com/advplyr/audiobookshelf/issues/3907)
* [@&#8203;adjokic](https://github.com/adjokic) made their first contribution in https://github.com/advplyr/audiobookshelf/pull/3884
* [@&#8203;tharvik](https://github.com/tharvik) made their first contribution in https://github.com/advplyr/audiobookshelf/pull/3906

[1.82.1]
* Update audiobookshelf to 2.19.1
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.19.1)
* Security issue for remote authentication bypass by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3963](https://github.com/advplyr/audiobookshelf/issues/3963) (thanks [@&#8203;swiftbird07](https://github.com/swiftbird07))
* Batch update after mapping empty fields shows no update necessary [#&#8203;3938](https://github.com/advplyr/audiobookshelf/issues/3938)
* Users with update permissions unable to remove books from collections [#&#8203;3947](https://github.com/advplyr/audiobookshelf/issues/3947)
* Server crash when feed cover image is requested and doesn't exist
* UI/UX: Book/episode edit modals: Next/prev buttons don't update contents of rich textarea when focused [#&#8203;3951](https://github.com/advplyr/audiobookshelf/issues/3951) in [#&#8203;3954](https://github.com/advplyr/audiobookshelf/issues/3954)
* UI/UX: Episode edit modal: Next/prev buttons causes 500 error (when opened on home page) (see [#&#8203;3954](https://github.com/advplyr/audiobookshelf/issues/3954))
* Performance improvement: book library page queries for title and addedAt sort [#&#8203;2073](https://github.com/advplyr/audiobookshelf/issues/2073) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3952](https://github.com/advplyr/audiobookshelf/issues/3952)
* Requests for podcast feeds includes `'Accept-Encoding': 'gzip, compress, deflate'` header [#&#8203;3885](https://github.com/advplyr/audiobookshelf/issues/3885) by [@&#8203;Vynce](https://github.com/Vynce) in [#&#8203;3941](https://github.com/advplyr/audiobookshelf/issues/3941)
* Better parsing of name strings with single name authors when using semicolon delimiter (and no commas) [#&#8203;3940](https://github.com/advplyr/audiobookshelf/issues/3940)
* UI/UX: Trim whitespace on blur from text inputs in book/podcast/episode edit modals and batch edit page [#&#8203;3943](https://github.com/advplyr/audiobookshelf/issues/3943) in [#&#8203;3946](https://github.com/advplyr/audiobookshelf/issues/3946)
* UI/UX: Book rich text description is now resizable [#&#8203;3928](https://github.com/advplyr/audiobookshelf/issues/3928) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3929](https://github.com/advplyr/audiobookshelf/issues/3929)
* UI/UX: Collection/Playlist and Batch Quick Match modals bg color consistent with other modals
* UI/UX: View feed modal on feed config page now shows feed episodes in order of pubDate
* More strings translated
* ROUTER_BASE_PATH env variable uses nullish coalescing operator by [@&#8203;devnoname120](https://github.com/devnoname120) in [#&#8203;3958](https://github.com/advplyr/audiobookshelf/issues/3958)
* [@&#8203;Vynce](https://github.com/Vynce) made their first contribution in https://github.com/advplyr/audiobookshelf/pull/3941

[1.82.2]
* Update audiobookshelf to 2.19.2
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.19.2)
* Server crash for some db queries using title/titleIgnorePrefix [#&#8203;3966](https://github.com/advplyr/audiobookshelf/issues/3966) in [#&#8203;3972](https://github.com/advplyr/audiobookshelf/issues/3972)

[1.82.3]
* Update audiobookshelf to 2.19.3
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.19.3)
* Caching issue with libraries page [#&#8203;3979](https://github.com/advplyr/audiobookshelf/issues/3979) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3993](https://github.com/advplyr/audiobookshelf/issues/3993)
* End of chapter sleep timer not always triggering [#&#8203;3969](https://github.com/advplyr/audiobookshelf/issues/3969) in [#&#8203;3984](https://github.com/advplyr/audiobookshelf/issues/3984)
* Server crash when matching all books where series sequence gets updated by match [#&#8203;3961](https://github.com/advplyr/audiobookshelf/issues/3961) in [#&#8203;3985](https://github.com/advplyr/audiobookshelf/issues/3985)
* UI/UX: Edit book modal cover tab: Local images overflowing [#&#8203;3986](https://github.com/advplyr/audiobookshelf/issues/3986)

[1.82.4]
* Update audiobookshelf to 2.19.4
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.19.4)
* Podcast feeds http to https redirect not working [#&#8203;3142](https://github.com/advplyr/audiobookshelf/issues/3142) [#&#8203;3658](https://github.com/advplyr/audiobookshelf/issues/3658) by [@&#8203;sloped](https://github.com/sloped) in [#&#8203;3978](https://github.com/advplyr/audiobookshelf/issues/3978)
* Sorting by title for book libraries not working on new databases [#&#8203;4003](https://github.com/advplyr/audiobookshelf/issues/4003) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;4005](https://github.com/advplyr/audiobookshelf/issues/4005)
* Improve podcast library performance [#&#8203;3965](https://github.com/advplyr/audiobookshelf/issues/3965) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;3996](https://github.com/advplyr/audiobookshelf/issues/3996)
* UI/UX: Refresh bookshelf when book title changes [#&#8203;3998](https://github.com/advplyr/audiobookshelf/issues/3998) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;4008](https://github.com/advplyr/audiobookshelf/issues/4008)
* More strings translated
* [@&#8203;sloped](https://github.com/sloped) made their first contribution in https://github.com/advplyr/audiobookshelf/pull/3978

[1.82.5]
* Update audiobookshelf to 2.19.5
* [Full Changelog](https://github.com/advplyr/audiobookshelf/releases/tag/v2.19.5)
* Caching issue with library items page [#&#8203;4018](https://github.com/advplyr/audiobookshelf/issues/4018) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;4020](https://github.com/advplyr/audiobookshelf/issues/4020)
* UI/UX: Long podcast author overflows in player by [@&#8203;smithmd](https://github.com/smithmd) in [#&#8203;3944](https://github.com/advplyr/audiobookshelf/issues/3944)
* API: Get podcast library items endpoint when not including a limit query param [#&#8203;4014](https://github.com/advplyr/audiobookshelf/issues/4014)
* API: Library delete, update and delete items with issues allowing calls from non-admin users by [@&#8203;Alexshch09](https://github.com/Alexshch09) in [#&#8203;4027](https://github.com/advplyr/audiobookshelf/issues/4027)
* Improved scanner ignore logic [#&#8203;2399](https://github.com/advplyr/audiobookshelf/issues/2399) [#&#8203;1641](https://github.com/advplyr/audiobookshelf/issues/1641) by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;4031](https://github.com/advplyr/audiobookshelf/issues/4031)
* Custom metadata provider requests log the url [#&#8203;4004](https://github.com/advplyr/audiobookshelf/issues/4004) by [@&#8203;nichwall](https://github.com/nichwall) in [#&#8203;4034](https://github.com/advplyr/audiobookshelf/issues/4034)
* UI/UX: Removing all items with issues redirects to library page [#&#8203;4022](https://github.com/advplyr/audiobookshelf/issues/4022) by [@&#8203;mikiher](https://github.com/mikiher) in [#&#8203;4037](https://github.com/advplyr/audiobookshelf/issues/4037)
* More strings translated
* [@&#8203;smithmd](https://github.com/smithmd) made their first contribution in https://github.com/advplyr/audiobookshelf/pull/3944
* [@&#8203;Alexshch09](https://github.com/Alexshch09) made their first contribution in https://github.com/advplyr/audiobookshelf/pull/4027

[1.83.0]
* Update base image to 5.0.0

