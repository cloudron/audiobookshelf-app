## THIS IS USING THE ORIGINAL PROJECT TO MAKE IT POSSIBLE TO DEPLOY IT IN CLOUDRON 

# Audiobookshelf
Package to deploy Audiobookshelf to Cloudron

The original source can be found in [Github](https://github.com/advplyr/audiobookshelf)

Required Changes are:
- Adding CloudronManifest.json
- Writing location for metadata and config to /app/data/audiobookshelf

## After first start the webpage will request username and password as master user - so configure this asap after starting up the App!

To deploy it manually on Cloudron follow this instruction:

Build the docker Image:
~~~
docker build -t APPNAME .
~~~

Tag the docker Image:
~~~
docker tag APPNAME:latest URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Push Image to registry:
~~~
docker push URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Install image on your Cloudron Instance:
~~~
cloudron install --image URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~
