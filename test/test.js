#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

const ADMIN_USERNAME = 'admin';
const ADMIN_PASSWORD = 'changeme';

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const LIBRARY_NAME = 'test library cloudron ' + Math.floor((Math.random() * 100) + 1);
    const LIBRARY_PATH = '/app/data/library_' + LIBRARY_NAME.replace(/\s/g, '_');
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/login');
        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[contains(., "Submit")]')).click();
        await waitForElement(By.xpath('//a[contains(., "Librar")]')); // Libraries or Library
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//a[contains(., "Login with")]'));
        await browser.findElement(By.xpath('//a[contains(., "Login with")]')).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//a[contains(., "Librar")]')); // Libraries or Library
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/audiobookshelf/account');

        await waitForElement(By.xpath('//button[contains(., "Logout")]'));
        await browser.findElement(By.xpath('//button[contains(., "Logout")]')).click();

        await browser.manage().deleteAllCookies();
        await waitForElement(By.xpath('//form/label[text()="Username"]/following-sibling::div[1]/input'));
    }

    async function checkLibrary() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath(`//button[contains(@aria-label, "Library:")]`));
        await browser.findElement(By.xpath(`//button[contains(@aria-label, "Library:")]`)).click();
        await waitForElement(By.xpath(`//ul[contains(@class, "librariesDropdownMenu")]/li[contains(., "${LIBRARY_NAME}")]`));
        await browser.findElement(By.xpath(`//ul[contains(@class, "librariesDropdownMenu")]/li[contains(., "${LIBRARY_NAME}")]`)).click();
        await waitForElement(By.xpath(`//p[contains(., "${LIBRARY_NAME}")]`));
    }

    async function addLibrary() {
        await browser.get('https://' + app.fqdn + '/config/libraries');

        await waitForElement(By.xpath('//button[contains(text(), "Add Library")]'));
        await browser.findElement(By.xpath('//button[contains(text(), "Add Library")]')).click();

        await waitForElement(By.xpath('//button[contains(text(),"Create")]'));
        await browser.findElement(By.xpath('//input[@placeholder="Library Name"]')).sendKeys(`${LIBRARY_NAME}`);
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//input[@placeholder="New folder path"]')).sendKeys(`${LIBRARY_PATH}`);
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//button[contains(text(),"Create")]')).click();
        await waitForElement(By.xpath(`//p[text()="${LIBRARY_NAME}"]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no SSO
    it('install app (no sso)', async function () {
        execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS);
        // wait when all settings take a place
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('can create project', addLibrary);
    it('check library', checkLibrary);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // SSO
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        // wait when all settings take a place
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can create project', addLibrary);
    it('check library', checkLibrary);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, username, password, false));
    it('check library', checkLibrary);
    it('can logout', logout);

    it('can restart app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        // wait when all settings take a place
        await browser.sleep(20000);
    });

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check library', checkLibrary);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, username, password));

    it('check library', checkLibrary);
    it('can logout', logout);

    it('backup app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS);
    });
    it('restore app', async function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        // wait when all settings take a place
        await browser.sleep(20000);
    });

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check library', checkLibrary);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, username, password));

    it('check library', checkLibrary);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        // wait when all settings take a place
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check library', checkLibrary);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, username, password));

    it('check library', checkLibrary);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', async function () {
        execSync(`cloudron install --appstore-id org.audiobookshelf.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        // wait when all settings take a place
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can login with username', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can create project', addLibrary);
    xit('check library', checkLibrary);
    it('can logout', logout);
    it('can user login OIDC', loginOIDC.bind(null, username, password));

    it('check library', checkLibrary);
    it('can logout', logout);

    it('can update', async function () {
        await browser.get('about:blank');
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        // wait when all settings take a place
        await browser.sleep(20000);
    });
    it('can get app information', getAppInfo);

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('check library', checkLibrary);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, username, password));
    it('check library', checkLibrary);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
