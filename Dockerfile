FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code

# renovate: datasource=github-releases depName=advplyr/audiobookshelf versioning=semver extractVersion=^v(?<version>.+)$
ARG AUDIOBOOKSHELF_VERSION=2.19.5

RUN curl -L https://github.com/advplyr/audiobookshelf/archive/refs/tags/v${AUDIOBOOKSHELF_VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code

WORKDIR /app/code/client
RUN npm ci && \
    npm cache clean --force && \
    npm run generate && \
    rm -rf node_modules

WORKDIR /app/code

ENV NODE_ENV=production
RUN npm ci --only=production

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
