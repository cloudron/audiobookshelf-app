FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code

# renovate: datasource=github-releases depName=advplyr/audiobookshelf versioning=semver extractVersion=^v(?<version>.+)$
ARG AUDIOBOOKSHELF_VERSION=2.19.5

RUN curl -L https://github.com/advplyr/audiobookshelf/archive/refs/tags/v${AUDIOBOOKSHELF_VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code

WORKDIR /app/code/client
RUN npm ci && \
    npm cache clean --force && \
    npm run generate && \
    rm -rf node_modules

WORKDIR /app/code

ENV NODE_ENV=production
RUN npm ci --only=production

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
